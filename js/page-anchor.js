$(function() {
  $('[data-js-page-anchor]').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('#' + this.hash.slice(1));
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top-65
        }, 600);
        return false;
      }
    }
  });
});
