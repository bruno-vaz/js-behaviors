$(window).scroll(function(){
  var s = $(window).scrollTop(),
      d = $(document).height(),
      c = $(window).height();
      scrollPercent = (s / (d-c)) * 100;
      var position = scrollPercent;
  $("[data-js-scroll-progress]").css('width', position+'%');
  if (s > 82){
   $('[data-js-blog-top]').addClass('-visible');
 } else{
   $('[data-js-blog-top]').removeClass('-visible');
 }
});
