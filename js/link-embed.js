var videoEmbed = {
	invoke: function(target) {
		$(target).html(function(i, html) {
			return videoEmbed.convertMedia(html);
		})
	},
	convertMedia: function(html) {
		var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
		var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=|embed\/)?(.+)/g;
		if (pattern1.test(html)) {
			var replacement = '<iframe src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
      var html = html.replace(pattern1, replacement);
		}
		if (pattern2.test(html)) {
			var replacement = '<iframe src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';
			var html = html.replace(pattern2, replacement);
		}
		return html;
	}
}
$(function(){
  videoEmbed.invoke('[data-js-link-embed]');
})
