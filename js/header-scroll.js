function changeHeaderState(state){
  var $target = $('[data-js-scroll-class]');
  if (state == 'solid'){
    $target.addClass('-scroll');
  } else if (state == 'transparent'){
    $target.removeClass('-scroll');
  }
}
function testScroll(){
  var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
  "use strict";
  if (scrollTop >= 10 ){
    changeHeaderState('solid');
  }
  else{
    changeHeaderState('transparent');
  }
}
window.onscroll = function(){
  testScroll()
};
$(function(){
  testScroll();
})
