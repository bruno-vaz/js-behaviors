function nextPage(url){
  $('[data-js-pagination-trigger]').addClass('-loading').html('CARREGANDO...');
  $.ajax({
    async: true,
    type: "POST",
    url: url,
    cache: false,
    success: function(html){
      if (html.length > 0) {
        var el = jQuery(html);
        var items = el.find('[data-js-masonry-item]');
        var button = el.find('[data-js-pagination-trigger]');
        $("[data-js-masonry-container]").append(items).masonry( 'appended', items, true );
        $('[data-js-load-btn]').html(button);
      }
    }
  });
}
$(document).on('click', '[data-js-pagination-trigger]', function(){
  var url = $(this).data('url');
  nextPage(url);
})
